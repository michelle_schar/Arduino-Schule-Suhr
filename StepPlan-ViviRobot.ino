 #include <Servo.h>

#define trigger 3
#define echo 2

Servo servo;
long timePassed = 0;
long distance = 0;

void setup() {
  servo.attach(4);
  Serial.begin(9600);
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
}

void loop() {
  scan();
}

void scan() {
  digitalWrite(trigger, LOW);
  delay(5);
  digitalWrite(trigger, HIGH);
  delay(10);
  digitalWrite(trigger, LOW);
  timePassed = pulseIn(echo, HIGH);
  distance = (timePassed/2)/29.1;
  Serial.println(distance);
  if(distance > 0 && distance < 12) {
    shakingHead();
  } else {
    Serial.println("Hindernis zu weit weg oder nicht vorhanden");
    }
  delay(1000);
}

void shakingHead() {
  servo.write(180);
  delay(1000);
  servo.write(0);
  delay(500);
}
