# Arduino Schule Suhr

Hier sind die Dokumentationen und ein Code Beipsiel für die geplante Projektwoche 2020 in der Schule Suhr abgelegt.
Geplant war es einen Vivirobot Stück für Stück zu erarbeiten und schliesslich zu programmieren.


### Arduino-Anfang-Aufgabe

Hier wird beschrieben wie man mit den Heuptbestandteilen von Arduino umgeht und für was Arduino eigentlich gut ist.
Zudem hat es drei kleine Einstiegsaufgaben mit Bildern und Beschrieb möglichst verständlich erklärt und am Schluss jeder 
Aufgabe auch noch ein Code Beispiel.

### Step-Plan-ViviRobot

Hier wird beschrieben was wir mit dem Vivirobot eigentlich erreichen wollen und was dieser alles kann. Es wird Schritt für Schritt
aufgebaut und die einzelnen Komponenten wie z.B. der Infrarotsensor werden mit allen dazugehörigen Klassen erklärt und es
wir verständlich gemacht wie diese funktionieren. Anschliessend gibt es zu jedem Komponenten eine kleine Einstiegsaufgabe, wieder 
mit Beschrieb, Bildern und Code Beispielen am Schluss.

Note: Die Aufgaben bauen zum Teil aufeinander auf, so soll sichergestellt werden, dass der Prozess vom Vivirobot verstanden wurde
und es soll gezeigt werden wie alles zusammenspielt.

### Code Example Arduino

Hier ist ein kelines Code Beispiel, wo der Roboter den Kopf schüttelt, wenn er ein "Hinderniss" ab einer gewissen Entfernung merkt.
